from hashlib import sha256
import json


class Block:
    def __init__(self, index, transactions, previous_hash, timestamp, nonce=0):
        self.index = index
        self.transactions = transactions
        self.previous_hash = previous_hash
        self.timestamp = timestamp
        self.nonce = nonce
        self.hash = self.get_hash()

    def get_hash(self):
        """Generates the hash of the block using sha256"""
        block_string = json.dumps(self.__dict__)
        return sha256(block_string.encode()).hexdigest()

