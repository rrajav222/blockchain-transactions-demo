import time
from block import Block
from transactions import Transaction
import json


class Blockchain:
    difficulty = 4

    def __init__(self):
        """Create the blockchain and then create the genesis block"""
        self.pending_transactions = []
        self.block_list = []
        self.create_genesis_block()

    def create_genesis_block(self):
        """Create the genesis block and add it to the chain"""
        genesis_block = Block(index=0, transactions=[],
                              previous_hash="0", timestamp=time.time())
        genesis_block.hash = genesis_block.get_hash()
        self.block_list.append(genesis_block)

    @property
    def last_block(self):
        """Returns the last block in the chain"""
        return self.block_list[-1]

    def add_new_transaction(self, transaction):
        t_json = json.dumps(transaction.__dict__)
        self.pending_transactions.append(t_json)

    def proof_of_work(self, block):
        """Generate a proof of work for the block"""
        block_hash = block.get_hash()
        while not block_hash.startswith('0' * Blockchain.difficulty):
            block.nonce += 1
            block_hash = block.get_hash()
        return block_hash

    def is_valid_proof(self, block, block_hash):
        """Returns true if the hash of the block is valid"""
        if block_hash.startswith('0' * Blockchain.difficulty) and block_hash == block.get_hash():
            return True
        return False

    def add_block_to_chain(self, block, proof):
        """Adds block to the chain of the block is valid"""
        previous_hash = self.last_block.hash
        if previous_hash != block.previous_hash:
            return False
        if not self.is_valid_proof(block, proof):
            return False
        block.hash = proof
        self.block_list.append(block)
        return True

    def maintain_balance(self, Users, transactions):
        """Changes the balance of each user based on the transactions after they are verified"""
        for item in transactions:
            transaction = json.loads(item)
            for user in Users:
                if transaction["sender"] == user.name:
                    user.balance -= transaction["amount"]
                if transaction["recipient"] == user.name:
                    user.balance += transaction["amount"]

    def mine(self, Users):
        """Mines a new block and adds it to the chain"""
        if not self.pending_transactions:
            print("no pending transactions")
            return False

        last_block = self.last_block

        new_block = Block(index=last_block.index + 1,
                          transactions=self.pending_transactions,
                          timestamp=time.time(),
                          previous_hash=last_block.hash)

        proof = self.proof_of_work(new_block)
        self.add_block_to_chain(new_block, proof)
        self.maintain_balance(Users, self.pending_transactions)
        self.pending_transactions = []
        return new_block.index
