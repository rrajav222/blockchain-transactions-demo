class User:
    def __init__(self, name, balance):
        self.name = name
        self.balance = balance
        
    def __str__(self):
        return "Name: {}, Balance: {}".format(self.name, self.balance)
