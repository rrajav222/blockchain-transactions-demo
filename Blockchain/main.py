from cgitb import text
from turtle import width
from blockchain import Blockchain
from transactions import Transaction
from users import User
import json
from tkinter import *
import random
BACKGROUND_COLOR = "#E4E4E4"
FONT = ('Times', 12)

chain = Blockchain()

Alice = User("Alice", 200)
Bob = User("Bob", 100)
Mark = User("Mark", 100)
Charlie = User("Charlie", 100)
Users = [Alice, Bob, Mark, Charlie]


def get_chain():
    chain_data = []
    for block in chain.block_list:
        chain_data.append(block.__dict__)
    return ({"length": len(chain_data),
             "chain": chain_data})


# if __name__ == '__main__':
window = Tk()
window.title("Blockchain")
window.geometry("1920x1080")
window.config(padx=50, pady=50, bg=BACKGROUND_COLOR)

# user frame
def update_account_frame():
    """updates the users account in the users box"""
    users_frame = LabelFrame(window, bd=5, text="Users",
                             font=FONT, bg=BACKGROUND_COLOR, padx=10, pady=10)
    users_frame.grid(row=0, column=0, padx=10, pady=10)

    user_label = {}
    for item in Users:
        user_label[item] = Label(
            users_frame, justify=LEFT, font=FONT, bg=BACKGROUND_COLOR, padx=10, pady=10, width=20)
        user_label[item].config(text=f"{item.name} \nBalance : {item.balance}")
        user_label[item].grid()


update_account_frame()

# transaction frame
transactionFrame = LabelFrame(
    window, bd=5, text="Perform Transaction", font=FONT, bg=BACKGROUND_COLOR, padx=10, pady=10)
transactionFrame.grid(row=0, column=1, padx=10, pady=10)

transaction_label_sender = Label(transactionFrame, text="Send money from :",
                                 justify=LEFT, font=FONT, bg=BACKGROUND_COLOR, padx=10, pady=10, width=20)
transaction_label_sender.grid(row=0, column=0, padx=10, pady=10)

sender_user = StringVar(transactionFrame)
userlistMenu = [item.name for item in Users]
sender_user.set("Select User")

sender_menu = OptionMenu(transactionFrame, sender_user, *userlistMenu)
sender_menu.config(font=FONT, width=10)
sender_menu.grid(row=1, column=0, padx=10, pady=10)


def callback(*args):
    """Callback function for the transaction dropdowns to select the sender and receiver"""
    transaction_label_sender.configure(
        text=f"Send money from : {sender_user.get()}")
    transaction_label_receiver.configure(
        text=f"Send money to : {receiver_user.get()}")


sender_user.trace("w", callback)


transaction_label_to = Label(transactionFrame, text="to",
                             font=FONT, bg=BACKGROUND_COLOR, padx=10, pady=10, width=2)
transaction_label_to.grid(row=0, column=1, padx=10, pady=10)

transaction_label_receiver = Label(transactionFrame, text="Send money from :",
                                   justify=LEFT, font=FONT, bg=BACKGROUND_COLOR, padx=10, pady=10, width=20)
transaction_label_receiver.grid(row=0, column=2, padx=10, pady=10)

receiver_user = StringVar(transactionFrame)
receiver_user.set("Select User")

receiver_menu = OptionMenu(transactionFrame, receiver_user, *userlistMenu)
receiver_menu.config(font=FONT, width=10)
receiver_menu.grid(row=1, column=2, padx=10, pady=10)

receiver_user.trace("w", callback)

amount_label = Label(transactionFrame, text="Amount:",
                     font=FONT, bg=BACKGROUND_COLOR, padx=10, pady=10, width=20)
amount_label.grid(row=2, column=1)

amount_entry = Entry(transactionFrame)
amount_entry.config(font=FONT, width=10, x=10)
amount_entry.grid(row=3, column=1)


def check_balance(sender):
    """Gets the user balance based on the sender name"""
    if sender == Alice.name:
        return Alice.balance
    if sender == Bob.name:
        return Bob.balance
    if sender == Mark.name:
        return Mark.balance
    if sender == Charlie.name:
        return Charlie.balance


def send_money():
    """Function called when send button is clicked"""
    try:
        amount = float(amount_entry.get())
    except ValueError:
        print("Please enter a valid amount")
    else:
        sender = sender_user.get()
        receiver = receiver_user.get()
        try:
            for item in Users:
                if item.name == sender_user.get():
                    sender = item.name
                if item.name == receiver_user.get():
                    receiver = item.name
            if check_balance(sender) >= amount:
                transaction = Transaction(sender, receiver, amount)
                chain.add_new_transaction(transaction)
                transaction_history_textbox.delete(1.0, END)
                for item in chain.pending_transactions:
                    transaction_history_textbox.insert(END, item + "\n")
            else:
                print("Not enough balance")
        except TypeError:
            print("Transaction requires a sender, receiver and valid amount")


gap = Label(transactionFrame, text="", bg=BACKGROUND_COLOR)
gap.grid(row=4, column=1)
send_button = Button(transactionFrame, text="Send", font=FONT,
                     bg=BACKGROUND_COLOR,  width=10, command=send_money)
send_button.grid(row=5, column=1)


# transaction histroy frame

transaction_history_frame = LabelFrame(
    window, bd=5, text="Pending Transactions", font=FONT, bg=BACKGROUND_COLOR, padx=10, pady=10)
transaction_history_frame.grid(row=0, column=2, padx=10, pady=10)
# latest_transaction_label = Label(
#    transaction_history_frame, text="Transactions", font=FONT, bg=BACKGROUND_COLOR, padx=10, pady=10)
# latest_transaction_label.grid(row=0, column=0)

transaction_history_textbox = Text(
    transaction_history_frame, width=70, height=14, font=('Times', 12), bg="#fff")
transaction_history_textbox.grid(row=1, column=0)


def mine_button_clicked():
    """Function called when mine button is clicked"""
    chain.mine(Users)
    update_blockchain(chain_box)
    update_account_frame()
    transaction_history_textbox.delete(1.0, END)
    print(json.dumps(get_chain()))


mine_button = Button(window, text="Mine", font=FONT,
                     bg=BACKGROUND_COLOR,  width=10, padx=10, pady=10, command=mine_button_clicked)
mine_button.grid(row=1, column=1)


# mine frame
blockchain_frame = LabelFrame(window, bd=5, text="Block Chain",
                              font=FONT, bg=BACKGROUND_COLOR, padx=10, pady=10, width=1000)
blockchain_frame.grid(row=2, column=0, padx=10, pady=10, columnspan=3)

chain_box = Text(
    blockchain_frame, width=200, font=('Times', 12), bg="#fff")
chain_box.grid()


def update_blockchain(chain_box):
    """Updates the blockchain textbox"""
    chain_box.delete(1.0, END)
    for i in range(len(chain.block_list)):
        item = chain.block_list[i].__dict__
        chain_box.insert(END, str(item) + "\n")
        chain_box.grid()


update_blockchain(chain_box)

window.state('zoomed')
window.mainloop()
